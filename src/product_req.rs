use crate::product::Product;
use anyhow::Result;
use reqwest::blocking::Response;
use select::document::Document;
use select::predicate::Attr;
use std::convert::TryFrom;

pub fn get_products() -> Result<Response> {
    // Possible args: shop: u32, amount: Option<u32>
    // Other Search params:
    // &volume_lower=[0.0 TO *]
    // &productInStore=2469
    // &beerSubstyleId=beersubstyle_ale
    let res = reqwest::blocking::Client::new()
      .get("https://www.alko.fi/INTERSHOP/web/WFS/Alko-OnlineShop-Site/fi_FI/-/EUR/ViewParametricSearch-ProductPaging")
      .query(&[
          ("Context", "ViewParametricSearch-ProductPaging"),
          //("SearchTerm", "*"),
          //("PageSize", "12"), // Changing this does not change the response
          ("SortingAttribute", "name-asc"), // This should be set to get deterministic responses
          ("PageNumber", "0"),
          ("SearchParameter", "&@QueryTerm=*"),
          //("AjaxRequestMarker", "true")
      ])
      .send()?;
    Ok(res)
}
fn get_iter_products() {
    (0..).map(|page| {
    reqwest::blocking::Client::new()
      .get("https://www.alko.fi/INTERSHOP/web/WFS/Alko-OnlineShop-Site/fi_FI/-/EUR/ViewParametricSearch-ProductPaging")
      .query(&[
          ("Context", "ViewParametricSearch-ProductPaging"),
          //("SearchTerm", "*"),
          //("PageSize", "12"), // Changing this does not change the response
          ("SortingAttribute", "name-asc"), // This should be set to get deterministic responses
          ("PageNumber", page.to_string().as_str()),
          ("SearchParameter", "&@QueryTerm=*"),
          //("AjaxRequestMarker", "true")
      ])
      .send()
  })
  .map_while(|r| r.ok())
  .filter_map(|r| Document::from_read(r).ok())
  .map(|d| {
     let s = d.select(Attr("class", "product-data-container"));
     s.filter_map(|e| Product::try_from(e).ok())
  })
  .for_each(drop);
}
