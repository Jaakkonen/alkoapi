use std::convert::TryFrom;

use schemars::JsonSchema;
use select::node::Node;
use select::predicate::{Attr, Class, Name, Predicate};
use serde::{de, Deserialize, Deserializer, Serialize};

use crate::deserialization_util::*;
use anyhow::Error;

#[derive(Serialize, Deserialize, JsonSchema, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Product {
    #[serde(deserialize_with = "de_str_int")]
    id: u32,
    name: String,
    #[serde(deserialize_with = "de_str_float")]
    size: f32,
    selection: String,
    category: String,
    origin: String,
    supplier: String,
    producer: String,
    #[serde(deserialize_with = "de_str_float")]
    alcohol: f32,
    packaging: String,
    #[serde(deserialize_with = "de_alkobool")]
    green_choice: bool,
    #[serde(deserialize_with = "de_alkobool")]
    ethical: bool,
    // Fields that are not in data-product-data json
    #[serde(skip_deserializing)]
    image: String,
    #[serde(skip_deserializing)]
    price: f32,
    #[serde(skip_deserializing)]
    link: String,
}

impl TryFrom<Node<'_>> for Product {
    type Error = Error;

    fn try_from(n: Node) -> Result<Self, Self::Error> {
        let data_product_data = n
            .attr("data-product-data")
            .ok_or(Error::msg("Product data not found"))?;
        let mut product_data: Product = serde_json::from_str(data_product_data)?;
        product_data.image = n
            .select(Name("imgproduct"))
            .next()
            .ok_or(Error::msg("imgproduct element not found for image data"))?
            .attr("src")
            .ok_or(Error::msg("Image src not found"))?
            .trim_start()
            .to_string();
        product_data.price = n
            .select(Attr("itemprop", "price"))
            .next()
            .ok_or(Error::msg("Element with itemprop=price not found"))?
            .attr("content")
            .ok_or(Error::msg("No price data content"))?
            .parse()?;
        product_data.link = n
            .select(Class("js-product-link"))
            .next()
            .ok_or(Error::msg("a element of class js-product-link not found"))?
            .attr("href")
            .ok_or(Error::msg("js-product-link did not have href"))?
            .to_string();
        println!("{:#?}", n);
        Ok(product_data)
    }
}
