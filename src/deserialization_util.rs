use serde::{de, Deserialize, Deserializer};

pub fn de_str_int<'de, D>(deserializer: D) -> Result<u32, D::Error>
where
    D: Deserializer<'de>,
{
    let s = <&str>::deserialize(deserializer)?;
    s.parse().map_err(de::Error::custom)
}
pub fn de_str_float<'de, D>(deserializer: D) -> Result<f32, D::Error>
where
    D: Deserializer<'de>,
{
    <&str>::deserialize(deserializer)?
        .parse()
        .map_err(de::Error::custom)
}
pub fn de_alkobool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    let s = <&str>::deserialize(deserializer)?;
    match s {
        "no" => Ok(false),
        "yes" => Ok(true),
        _ => Err(de::Error::custom("alkobool invalid value")),
    }
}
