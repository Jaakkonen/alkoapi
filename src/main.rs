#![feature(proc_macro_hygiene, decl_macro)]
#![feature(iter_map_while)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_okapi;
extern crate reqwest;

use rocket_contrib::json::Json;
use rocket_okapi::swagger_ui::*;
use select::document::Document;
use select::predicate::{Attr, Class, Name, Predicate};
use std::convert::TryFrom;

mod product;
use product::Product;

mod shop;
use shop::Shop;

mod product_req;
use anyhow::Result;
use product_req::get_products;

mod deserialization_util;

#[openapi]
#[get("/shops")]
fn get_all_shops() -> Json<Vec<Shop>> {
    Json(vec![])
}

#[openapi]
#[get("/product")]
fn get_all_products() -> Result<Json<Vec<Product>>> {
    let res = get_products()?;
    let doc = Document::from_read(res)?;
    let products = //aamuja
        doc
        .select(Name("div"))
        //.map(|f| { println!("{:#?}", f.attr("class")); f})
        .filter(|n| n.attr("class") == Some("product-data-container"))
        .filter_map(|n| {
            match Product::try_from(n) {
                Ok(p) => Some(p),
                Err(e) => { println!("{:#?}", e); None}
            }
        });

    Ok(Json(products.collect()))
}

fn main() {
    rocket::ignite()
        .mount("/", routes_with_openapi![get_all_shops, get_all_products])
        .mount(
            "/swagger-ui/",
            make_swagger_ui(&SwaggerUIConfig {
                url: "../openapi.json".to_owned(),
                ..Default::default()
            }),
        )
        .launch();
}
